<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <?php if(Yii::app()->user->isGuest) { ?>
    <div class="span12">
    <?php } else { ?>
    <div class="span9">    
    <?php } ?>
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
    <?php if(!Yii::app()->user->isGuest) { ?>
    <div class="span3">
        <div id="sidebar">
        <?php
            $this->beginWidget('zii.widgets.CPortlet', array(
                'title'=>'Operações',
            ));
            $this->widget('bootstrap.widgets.TbMenu', array(
                'items'=>$this->menu,
                'htmlOptions'=>array('class'=>'operations'),
            ));
            $this->endWidget();
        ?>
        </div><!-- sidebar -->
    </div>
    <?php } ?>
</div>
<?php $this->endContent(); ?>