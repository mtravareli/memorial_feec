<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="pt-br" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'Pessoas','items'=>array(
                    array('label'=>'Alunos', 'url'=>array('/student/index')),
                    array('label'=>'Funcionários', 'url'=>array('/employee/index')),
                    array('label'=>'Professores', 'url'=>array('/professor/index')),
                    '---',
                    array('label'=>'Todos', 'url'=>array('/person/index')),

                )),
                array('label'=>'Sobre', 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>'Contribua', 'url'=>array('/site/contributions')),
                array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Gerenciar', 'visible'=>!Yii::app()->user->isGuest,'items'=>array(
                    array('label'=>'Pessoas', 'url'=>array('/person/admin')),
                    array('label'=>'Usuários', 'url'=>array('/user/index')),
                    array('label'=>'Ver contribuições', 'url'=>array('/personDraft/index')),
                    '---',
                    array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout')),
                )),
            ),
        ),
        '<form class="navbar-search pull-right" method="post" action="'.Yii::app()->createUrl('site/search').'"><input name="query" type="text" class="search-query span2" placeholder="Buscar"></form>',
    ),
)); ?>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by FEEC Unicamp.<br/>
		Todos os direitos reservados.<br/>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
