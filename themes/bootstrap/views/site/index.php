<div>

    <?php
    /* @var $this SiteController */

    $this->pageTitle=Yii::app()->name;
    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerCoreScript('jquery.ui'); 
    ?>


    <?php
    $this->renderPartial('_timeline',array(
                            //'dataProvider'=>$dataProvider,
                            'timeline' => $timeline,
                            'startDate' => $startDate,
                            'endDate' => $endDate,
                    ));
    ?>

    <br>

    <div id="timeline" style="position:relative;">
        <div id="leftTimeline" style="position:absolute; left:0; min-width: 46%; max-width: 46%;"></div>  
        <div id="rightTimeline" style="position:absolute; right:0; min-width: 46%; max-width: 46%;">
            <br><br>
        </div>
    </div>     
    <br>

</div>


    <?php
    $i=0;
    foreach ($timeline as $item){

        if (isset($item['person'])){
            $model = $item['person'];
            $link = $this->createUrl('person/view',array('id'=>$model->id));
        } else {
            $model = $item['testimonial']; 
            $link = $this->createUrl('person/view',array('id'=>$model->idPerson));
        } 
        $date = $item['date'];
        $event = $item['event'];

        if ($i%2 !== 0){
            $side = 'right';
        } else {
            $side = 'left';
        }
        
        $widget =   json_encode($this->widget("widgets.PreviewTimelineWidget",array(
                        'model' => $model,
                        'date' => $date,
                        'event' => $event,
                        'side' => $side,
                        'link' => $link,
        ),true));
        
        ?>
            <script>
                $('#<?php echo $side?>Timeline').append(<?php echo $widget?>+'<br>');
            </script>   
            
        <script>
            $("<?php echo '#zoomin'.str_replace('/', '-',$date); ?>").click(function() {
                 var date = <?php echo substr($date,6,9); ?>;
                 $( "#startDate" ).val(date-1);
                 $( "#endDate" ).val(date+1);
                 $("#selectRangeDate").submit();
            });
        </script>            
    <?php  
        $i++;
    }
    ?> 

    <script>
    $( document ).ready(function() {
        var height = <?php echo ($i/2) ?>;
        height = 110*height + 110;
        $('#timeline').css('height',height);
        $('body').css("background-color" ,"#EEEEEE");
    });
    </script>               