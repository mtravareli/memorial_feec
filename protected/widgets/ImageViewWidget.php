<?php
/**
 * Widget to show image
 * @example
 *
 *   $this->widget('ext.imageAttachment.imageAttachmentWidget', array(
 *       'model' => $model,
 *       'behaviorName' => 'previewImageAttachmentBehavior',
 *   ));
 *
 * @author Diego Zilioti
 */
class ImageViewWidget extends CWidget
{
    public $assets;

    /**
     * Behaviour name in model to use
     * @var string
     */
    public $behaviorName;

    /**
     * Model with behaviour
     * @var CActiveRecord
     */
    public $model;

    /**
     * @return ImageAttachmentBehavior
     */
    public function getBehavior()
    {
        return $this->model->{$this->behaviorName};
    }

    public function init()
    {
        $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets');
    }

    public function run()
    {
        /** @var $cs CClientScript */
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile($this->assets . '/imageAttachment.css');

        $cs->registerCoreScript('jquery');

        $cs->registerScriptFile($this->assets . '/jquery.iframe-transport.js');
        $cs->registerScriptFile($this->assets . '/jquery.imageView.js');

        $options = array(
            'hasImage' => $this->behavior->hasImage(),
            'previewUrl' => $this->behavior->getUrl('preview'),
            'previewWidth' => $this->behavior->previewWidth,
            'previewHeight' => $this->behavior->previewHeight,
        );

        if (Yii::app()->request->enableCsrfValidation) {
            $options['csrfTokenName'] = Yii::app()->request->csrfTokenName;
            $options['csrfToken'] = Yii::app()->request->csrfToken;
        }

        $optionsJS = CJavaScript::encode($options);
        $cs->registerScript('imageAttachment#' . $this->id, "$('#{$this->id}').imageAttachment({$optionsJS});");

        $this->render('imageView');
    }


}