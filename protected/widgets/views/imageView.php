<?php
/**
 * @var ImageViewWidget $this
 */

?>
<div class="imageAttachment" id="<?php echo $this->id ?>">
    <div class="preview">
        <div class="no-image"></div>
        <img/>
    </div>
</div>

