<div class="previewTimeline" style="position: relative;">
    
    <?php if(strcmp('left', $side) == 0) { ?>
       <div class="talkbubbleleft <?php echo $eventType ?>">
    <?php } else { ?>
        <div class="talkbubbleright <?php echo $eventType ?>">
    <?php } ?>
            <a href='<?php echo $link ?>'>
            <div class ="row-fluid">
            <?php if(strcmp('left', $side) == 0) { ?>
                <div class="span9">
                    <?php echo CHtml::tag('h4', array('id'=>'eventText'), $eventText, true) ?>                
                </div>
                <div class="time span3">
                    <?php echo CHtml::tag('h4', array('id'=>'date'), $date, true) ?>                
                </div>  
            <?php } else { ?> 
                <div class="time span3">
                    <?php echo CHtml::tag('h4', array('id'=>'date'), $date, true) ?>                
                </div>                  
                <div class="span9">
                    <?php echo CHtml::tag('h4', array('id'=>'eventText'), $eventText, true) ?>                
                </div>
            <?php } ?>                   
              
            </div>        
            <div class ="row-fluid" style="position:relative;">
            <?php if(strcmp($eventType,'testimonial') === 0) { ?>
                <div class="span8">
                    <?php echo CHtml::tag('p', array('id'=>'eventTestimonialText'), substr($model->text,0,100).'...', true) ?>       
                </div>
                <div class="span4" style="position:absolute;right:0;bottom:0;">
                    <?php echo CHtml::tag('p', array('id'=>'eventTestimonialName'), 'por '.$model->name, true) ?>       
                </div>   
            <?php } else { ?>
                <div class="image span2">
                    <div class="row-fluid">
                    <?php if($model->preview->hasImage()) { ?>
                        <div class="media span12" style="text-align:center">
                                <img id="logo" class="media-object" src="./images/profile/preview/<?php echo $model->id ?>.jpg">     
                        </div>  
                    <?php } else { ?>
                        <div class="media span12" style="text-align:center">
                                <img id="logo" class="media-object" src="./images/profile/preview/default.jpg">     
                        </div>                          
                    <?php } ?>    
                    </div>                  

                </div>

                <div class="span10">
                    <div class="row-fluid"> 
                        <div class="title span4">
                            <?php echo CHtml::tag('h5', array('id'=>'title'), $model->firstName.' '.$model->middleName.' '.$model->lastName, true) ?>
                        </div>
                        <div class="information span8">
                            <?php if(strcmp($event,'Student') === 0 && isset($model->firstStudent->course)) { ?>
                            <?php echo CHtml::tag('h6', array('id'=>'title'), $model->firstStudent->course, true) ?>
                            <?php } ?>
                            <?php if(strcmp($event,'Professor') === 0 && isset($model->firstProfessor->department)) { ?>
                            <?php echo CHtml::tag('h6', array('id'=>'title'), $model->firstProfessor->department, true) ?>
                            <?php } ?>
                            <?php if(strcmp($event,'Employee') === 0 && isset($model->firstEmployee->job)) { ?>
                            <?php echo CHtml::tag('h6', array('id'=>'title'), $model->firstEmployee->job, true) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?> 
            </div>
            </a>
        </div>
    <?php if(strcmp('left', $side) == 0) { ?>
       <div id="<?php echo 'zoomin'.str_replace('/', '-',$date); ?>" class ="zoominleft" title="Zoom in">
    <?php } else { ?>
       <div id="<?php echo 'zoomin'.str_replace('/', '-',$date); ?>" class ="zoominright" title="Zoom in">
    <?php } ?>           
        <?php $this->widget('bootstrap.widgets.TbButton', array('size'=>'mini', 'icon'=>'icon-zoom-in')); ?>
    </div>                    
</div>








