<?php

class PreviewTimelineWidget extends CWidget
{
    public $assets;

    /**
     * Model with behaviour
     * @var CActiveRecord
     */
    public $model;    
    
    /*
     * @var string
     * Side of previewWidget in the timeline
     * required (Default: left)
     */
    public $side = 'left';

    /*
     * @var string
     * Link to where widget will redirect
     * required
     */
    public $link;
    
    /*
     * @var string
     * Date of the event
     * required
     */
    public $date;
    
    /*
     * @var string
     * Event text to be displayed
     * required
     */
    public $event;
    
    /*
     * @var string
     * Type of event (death, in, out)
     * calculated in the widget
     */
    public $eventType; 
    
    /*
     * @var string
     * Type of event (death, in, out)
     * calculated in the widget
     */
    public $eventText;      
    
    public function init()
    {         
        
        if($this->assets===null)
        {
            $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets');
        }
        Yii::app()->clientScript->registerCssFile($this->assets . '/previewTimeline.css');
        
        if (strcmp('death:Person',$this->event) === 0) {
            $this->eventText = 'Faleceu uma pessoa';
            $this->eventType = 'death';
            $this->event = 'Person';
        } else if (strcmp('in:Professor',$this->event) === 0) {
            $this->eventText = 'Entrou um novo professor';
            $this->eventType = 'in';
            $this->event = 'Professor';
        } else if (strcmp('out:Professor',$this->event) === 0) {
            $this->eventText = 'Um professor deixou a faculdade';
            $this->eventType = 'out';
            $this->event = 'Professor';
        } else if (strcmp('in:Student',$this->event) === 0) {
            $this->eventText = 'Ingressou um novo aluno';
            $this->eventType = 'in';
            $this->event = 'Student';
        } else if (strcmp('out:Student',$this->event) === 0) {
            $this->eventText = 'Um aluno deixou a faculdade';
            $this->eventType = 'out';
            $this->event = 'Student';
        } else if (strcmp('in:Employee',$this->event) === 0) {
            $this->eventText = 'Entrou um novo funcionário';
            $this->eventType = 'in';
            $this->event = 'Employee';
        } else if (strcmp('out:Employee',$this->event) === 0) {
            $this->eventText = 'Um funcionário deixou a faculdade';
            $this->eventType = 'out';
            $this->event = 'Employee';
        } else if (strcmp('testimonial',substr($this->event,0,11)) === 0) {
            $this->eventText = substr($this->event,12).' recebeu um depoimento';
            $this->eventType = 'testimonial';
        }         

    }
 
    public function run()
    {
        
        $this->render('previewTimeline',array(
            'model' => $this->model,
            'date' => $this->date,
            'event' => $this->event,
            'eventText' => $this->eventText,
            'eventType' => $this->eventType,
            'side' => $this->side,
            'link' => $this->link,
        ));
    }
}

?>