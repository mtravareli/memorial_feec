<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Usuários'=>array('index'),
	'Criar',
);

$this->menu=array(
	array('label'=>'Listar Usuários', 'url'=>array('index')),
);
?>

<h1>Criar Usuário</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>