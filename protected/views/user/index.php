<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Usuários',
);

$this->menu=array(
	array('label'=>'Criar Usuário', 'url'=>array('create')),
);
?>

<h1>Usuários</h1>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
        'emptyText'=>"Não há usuários que satisfaçam essa busca.",
        'summaryText'=>'Total: {count}',
	'filter'=>$model,
	'columns'=>array(         
                'id',
                'username',
                 array(
                       'header' => 'Excluir',
                       'class'=>'CButtonColumn',
                       'template'=>'{delete}',
                       'buttons'=>array(
                           'view'=>array(
                               'url'=>'Yii::app()->createUrl("/user/delete")',
                           )
                       )
                 ),            
            ),
));
?>
