<?php
/* @var $this ProfessorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Professores',
);
?>

<h1>Professores</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */

$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
        'emptyText'=>"Não há funcionários que satisfaçam essa busca.",
        'summaryText'=>'Total: {count}',
	'filter'=>$model,
	'columns'=>array(
 
            
                array(
                    'name'=>'person_search',
                    'header' => 'Nome',
                    'value'=>'$data->idPerson0->firstName . " " . $data->idPerson0->middleName . " " . $data->idPerson0->lastName', 
                ),
                'researchArea',
                'department',
                'academicFormation',                
               
                 
                array(
                       'header' => 'Ver',
                       'class'=>'CButtonColumn',
                       'template'=>'{view}',
                       'buttons'=>array(
                           'view'=>array(
                               'url'=>'Yii::app()->createUrl("/person/$data->idPerson")',
                           )
                       )
                   ),
            ),
    
));
?>

