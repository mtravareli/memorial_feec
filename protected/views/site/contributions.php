<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contribuições';
$this->breadcrumbs=array(
	'Contribuições',
);
?>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block'=>true, // display a larger alert block?
    'fade'=>true, // use transitions?
    'closeText'=>'×', // close link text - if set to false, no close link is displayed
    'alerts'=>array( // configurations per alert type
	    'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
        ),
)); 
?>

<h1>Quer contribuir?</h1>

<h2>Escolha a forma: </h2>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'label' => 'Informações de pessoa',
        'type' => 'primary',
        'url' => $this->createUrl('personDraft/create'),
    )
);
echo '&#09;';
$this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'label' => 'Fale conosco',
        'type' => 'primary',
        'url' => $this->createUrl('site/contact'),
    )
);
?>

<br>

<br>