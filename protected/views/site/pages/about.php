<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Sobre';
$this->breadcrumbs=array(
	'Sobre',
);
?>
<h1>Sobre</h1>


<h4>PLATAFORMA EM MEMÓRIA DA FEEC </h4>
<p>
A história de uma faculdade, assim como de uma civilização ou nação, define seu presente e futuro. 
Para que se possa entender o presente, é importante saber o processo de desenvolvimento que originou a atual situação. 
Porém, como obter esse conhecimento uma vez que não existe registro formal dessa história? 
Como valorizar o trabalho de quem passou pela faculdade e deixá-lo para futuras gerações?
Essa necessidade de criar uma memória das pessoas, das descobertas científicas, das entidades e dos eventos que definiram o atual curso da Faculdade de Engenharia Elétrica e de Computação (FEEC) nos levou à idealização de um projeto.
</p>
<p>
Tal projeto se iniciou com a formação de um grupo de trabalho composto por nós (Diego de Sousa e Murilo Travareli), pelos Profs. Eduardo Alves do Valle Jr. e Romis Attux e pelos discentes Alan Godoy de Souza Mello (doutorado) e Marco Antonio Lasmar Almada (graduação – Engenharia de Computação). 
Esse grupo realizou diversas discussões nas quais começou a tomar forma nítida uma ideia: criar uma página web que reunisse informações sobre os professores e funcionários da FEEC que já não estão mais na ativa e suas contribuições ao desenvolvimento da faculdade. 
A possibilidade de, por exemplo, inserir informações e depoimentos a respeito de aspectos da vida, da carreira e da atuação de cada professor foi cogitada.   
</p>
