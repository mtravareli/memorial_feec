<?php 
// uncomment to test return from select time
//foreach ($timeline as $var) {    
    //echo $var['person']->id . $var['person']->firstName . "   ".$var['date'] ." ".$var['event'] ."<br/>";
//}
?>

  
<script>
  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 1950,
      max: 2020,
      values: [ <?php echo $startDate; ?>, <?php echo $endDate; ?> ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
        $( "#startDate" ).val( ui.values[0]); 
        $( "#endDate" ).val( ui.values[1]);
      }
    });
    $( "#amount" ).val( "" + $( "#slider-range" ).slider( "values", 0 ) +
      " - " + $( "#slider-range" ).slider( "values", 1 ) );
      
    $( "#startDate" ).val( $( "#slider-range" ).slider( "values", 0 )); 
    $( "#endDate" ).val( $( "#slider-range" ).slider( "values", 1 ));
    
  });
  </script>
  <p style="text-align: center;">
  <label style="color: #b2b2b2; font-weight: bold;" for="amount">Intervalo de tempo:</label>
  <input type="text" id="amount" style="border: 0; color: #BFBFBF; font-weight: bold; text-align: center; background-color: white;" readonly />
</p>
<div id="slider-range"></div>


<form id="selectRangeDate" style="margin-top: 10px;"class="navbar-search pull-right" method="POST" action="<?php echo Yii::app()->createUrl('/site/index') ?>">
    <input id="startDate" name="startDate" type="hidden" value=""/>
    <input id="endDate" name="endDate" type="hidden" value=""/>
    <button type="submit" class="btn btn-primary">Visualizar</button>
</form>

<br/>
<br/>