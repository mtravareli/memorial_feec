<?php
/* @var $this SiteController */
/* @var $queryText QueryText */

$this->pageTitle=Yii::app()->name . ' - Busca';
$this->breadcrumbs=array(
	'Busca',
);
?>

<h1>Busca
    <br>
        <form class="navbar-search pull-left" action="<?php echo Yii::app()->createUrl('/site/search') ?>">
            <input name="query" type="text" class="search-query" placeholder="<?php echo $queryText ?>"/>
            <button type="submit" class="btn">Buscar</button>
        </form>
    </br>
</h1>

<br>
<h2>Pessoas</h2>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
        'emptyText'=>"Não há pessoas que satisfaçam essa busca.",
        'summaryText'=>'Total: {count}',
	//'template'=>"{items}",
	'columns'=>array(
            
              array(
                    'name'=>'name_search',
                    'header' => 'Nome',
                    'value'=>'$data->firstName . " " . $data->middleName . " " . $data->lastName', 
                ),
            
		//'id',
		//'firstName',
		//'middleName',
		//'lastName',
                'contact',
                //'is_student',
            
      
            
                array(
                    'name'=>'is_student',
                    'header' => 'Aluno',
                    'value'=>'isset(Student::Model()->findByAttributes(array("idPerson"=>$data->id))->class) ? "sim" : "não"', 
                    ),
            
                array(
                    'name'=>'is_employee',
                    'header' => 'Funcionário',
                    'value'=>'isset(Employee::Model()->findByAttributes(array("idPerson"=>$data->id))->job) ? "sim" : "não"', 
                    ),
            
               array(
                    'name'=>'is_professor',
                    'header' => 'Professor',
                    'value'=>'isset(Professor::Model()->findByAttributes(array("idPerson"=>$data->id))->registry) ? "sim" : "não"', 
                    ), 
                 
                array(
                       'header' => 'Ver',
                       'class'=>'CButtonColumn',
                       'template'=>'{view}',
                       'buttons'=>array(
                           'view'=>array(
                               'url'=>'Yii::app()->createUrl("person/view",array("id"=>$data->id))',
                           )
                       )
                   ),
            ),
    
));
?>