<?php
/* @var $this StudentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Alunos',
);

?>

<h1>Alunos</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */

//http://www.yiiframework.com/wiki/281/searching-and-sorting-by-related-model-in-cgridview/
$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
        'emptyText'=>"Não há alunos que satisfaçam essa busca.",
        'summaryText'=>'Total: {count}',
	'filter'=>$model,
	'columns'=>array(
  
                array(
                    'name'=>'person_search',
                    'header' => 'Nome',
                    'value'=>'$data->idPerson0->firstName . " " . $data->idPerson0->middleName . " " . $data->idPerson0->lastName', 
                ),
            
                'nickname',
                'course',
                'class',
                'academicEntities',
         
                 
                array(
                       'header' => 'Ver',
                       'class'=>'CButtonColumn',
                       'template'=>'{view}',
                       'buttons'=>array(
                           'view'=>array(
                               'url'=>'Yii::app()->createUrl("/person/$data->idPerson")',
                           )
                       )
                   ),
            ),
    
));
?>