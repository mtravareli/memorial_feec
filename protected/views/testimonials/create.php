<?php
/* @var $this TestimonialsController */
/* @var $model Testimonials */

$this->breadcrumbs=array(
	'Depoimento',
);
?>

<h1>Criar depoimento</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>