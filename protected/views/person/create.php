<?php
/* @var $this PersonController */
/* @var $model Person 
 * @var $student Student
 * @var $employee Employee
 * @var $professor Professor
 */

$this->breadcrumbs=array(
	'Pessoas'=>array('index'),
	'Adicionar',
);

$this->menu=array(
	array('label'=>'Gerenciar Pessoas', 'url'=>array('admin')),
);
?>

<h1>Adicionar Pessoa</h1>


<?php $this->renderPartial('_newform', array('model'=>$model,'student'=>$student, 'professor'=>$professor, 'employee'=>$employee)); ?>