<?php
/* @var $this PersonController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pessoas',
);

?>

<h1>Pessoas</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */

$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
        'emptyText'=>"Não há pessoas que satisfaçam essa busca.",
        'summaryText'=>'Total: {count}',
	//'template'=>"{items}",
	'filter'=>$model,
	'columns'=>array(
            
              array(
                    'name'=>'name_search',
                    'header' => 'Nome',
                    'value'=>'$data->firstName . " " . $data->middleName . " " . $data->lastName', 
                ),
            
		//'id',
		//'firstName',
		//'middleName',
		//'lastName',
                'contact',
                //'is_student',
            
      
            
                array(
                    'name'=>'is_student',
                    'header' => 'Aluno',
                    'value'=>'isset(Student::Model()->findByAttributes(array("idPerson"=>$data->id))->class) ? "sim" : "não"', 
                    //// causes 'Trying to get property of non-object' - no error if $data->city
                     'filter'=>CHtml::activeDropDownList($model, 'is_student', array('2' => 'todos','0' => 'não', '1' => 'sim')),
                    //'filter'=>CHtml::activeDropDownList
                    //'value'=>'CHtml::checkBox("is_student",null,array("value"=>$data->id,"id"=>"cid_".$data->id))',
                    ),
            
                array(
                    'name'=>'is_employee',
                    'header' => 'Funcionário',
                    'value'=>'isset(Employee::Model()->findByAttributes(array("idPerson"=>$data->id))->job) ? "sim" : "não"', 
                    //// causes 'Trying to get property of non-object' - no error if $data->city
                     'filter'=>CHtml::activeDropDownList($model, 'is_employee', array('2' => 'todos','0' => 'não', '1' => 'sim')),
                    ),
            
               array(
                    'name'=>'is_professor',
                    'header' => 'Professor',
                    'value'=>'isset(Professor::Model()->findByAttributes(array("idPerson"=>$data->id))->registry) ? "sim" : "não"', 
                    //// causes 'Trying to get property of non-object' - no error if $data->city
                     'filter'=>CHtml::activeDropDownList($model, 'is_professor', array('2' => 'todos','0' => 'não', '1' => 'sim')),
                    ), 
                 
                array(
                       'header' => 'Ver',
                       'class'=>'CButtonColumn',
                       'template'=>'{view}',
                       'buttons'=>array(
                           'view'=>array(
                               'url'=>'Yii::app()->createUrl("/person/$data->id")',
                           )
                       )
                   ),
            ),
    
));
?>