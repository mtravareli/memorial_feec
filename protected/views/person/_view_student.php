<?php
/* @var $this PersonController */
/* @var $model Student */
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'enterDate',
		'exitDate',
		'nickname',
		'course',
		'class',
		'academicEntities',
		'ra',
    ),
));
?>
