<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
)); ?>
<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block'=>true, // display a larger alert block?
    'fade'=>true, // use transitions?
    'closeText'=>'×', // close link text - if set to false, no close link is displayed
    'alerts'=>array( // configurations per alert type
	    'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'), // success, info, warning, error or danger
            'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
            'error1'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
            'error2'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
            'error3'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
        ),
));

?>
 
<fieldset>
 
    <legend>Informações gerais</legend>

    <table>
        <tr>
            <td align="right">
                <?php $this->widget('ext.imageAttachment.ImageAttachmentWidget', array(
                            'model' => $model,
                            'behaviorName' => 'preview',
                            'apiRoute' => 'site/saveImageAttachment',
                        ));
                ?>      
            </td>
            <td align="left">
                <?php echo $form->textFieldRow($model, 'firstName'); ?>
                <?php echo $form->textFieldRow($model, 'middleName'); ?>
                <?php echo $form->textFieldRow($model, 'lastName'); ?>
            </td>            
        </tr>
        <tr>
            <td colspan="2">
                <?php echo $form->datepickerRow($model, 'birthDate',
                    array('hint'=>'Clique dentro para abrir o calendário',
                    'prepend'=>'<i class="icon-calendar"></i>',
                    'options'=>array('format'=>'dd/mm/yyyy'))); ?>
                <?php echo $form->datepickerRow($model, 'deathDate',
                    array('hint'=>'Clique dentro para abrir o calendário',
                    'prepend'=>'<i class="icon-calendar"></i>',
                    'options'=>array('format'=>'dd/mm/yyyy'))); ?>
                <?php echo $form->textFieldRow($model, 'contact'); ?>
                <?php echo $form->redactorRow($model, 'career', array('class'=>'span8', 'rows'=>5)); ?>
                <?php echo $form->redactorRow($model, 'observations', array('class'=>'span8', 'rows'=>5)); ?>
                <?php echo $form->radioButtonListInlineRow($model, 'priority', array('1','2','3')); ?>
            </td>
        </tr>
    </table>
    
    <br/>
    
    <legend>Informações específicas</legend>
    
 <?php $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs', 
     'tabs'=>array(
        array('label'=>'Aluno', 'content'=>$this->renderPartial('_form_student', array('student'=>$student,'form'=>$form ),true)),
        array('label'=>'Professor', 'content'=>$this->renderPartial('_form_professor', array('professor'=>$professor,'form'=>$form),true), 'active' =>true),
        array('label'=>'Funcionário', 'content'=>$this->renderPartial('_form_employee', array('employee'=>$employee,'form'=>$form),true)),
    ),
)); ?>
  

</fieldset>
 
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Adicionar')); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'label'=>'Limpar campos')); ?>
</div>
 
<?php $this->endWidget(); ?>