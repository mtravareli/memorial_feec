<h3>Aluno</h3>
<?php echo $form->toggleButtonRow($student, 'isValid') ?>
<?php echo $form->datepickerRow($student, 'enterDate',
array('hint'=>'Clique dentro para abrir o calendário',
'prepend'=>'<i class="icon-calendar"></i>',
'options'=>array('format'=>'dd/mm/yyyy'))); ?>
<?php echo $form->datepickerRow($student, 'exitDate',
array('hint'=>'Clique dentro para abrir o calendário',
'prepend'=>'<i class="icon-calendar"></i>',
'options'=>array('format'=>'dd/mm/yyyy'))); ?>
<?php echo $form->textFieldRow($student, 'nickname'); ?>
<?php echo $form->textFieldRow($student, 'course',
array('hint'=>'No caso de mais de um curso, separá-los por ponto e vírgula.')); ?>
<?php echo $form->textFieldRow($student, 'class',
array('hint'=>'Informe o ano em que iniciou o curso (ex. 2009,2010..) 
    na ordem em que foram apresentados acima e separados por ponto e vírgula.')); ?>
<?php echo $form->textFieldRow($student, 'academicEntities'); ?>
<?php echo $form->textFieldRow($student, 'ra'); ?>