<?php
/* @var $this PersonController */
/* @var $model Person */
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'birthDate',
		'deathDate',
		'contact',
    ),
));
?>
