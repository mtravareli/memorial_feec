<?php
/* @var $this PersonController */
/* @var $model Professor */
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'enterDate',
		'exitDate',
		'academicFormation',
		'researchArea',
		'department',
		'registry',            
    ),
));
?>
