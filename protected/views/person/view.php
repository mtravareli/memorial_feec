<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'Pessoas'=>array('index'),
	$model->firstName,
);

if (!Yii::app()->user->isGuest){
$this->menu=array(
	array('label'=>'Adicionar Pessoa', 'url'=>array('create')),
	array('label'=>'Editar Pessoa', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Deletar Pessoa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Gerenciar Pessoas', 'url'=>array('admin')),
);
}
?>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block'=>true, // display a larger alert block?
    'fade'=>true, // use transitions?
    'closeText'=>'×', // close link text - if set to false, no close link is displayed
    'alerts'=>array( // configurations per alert type
	    'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
        ),
));

?>

<table>
    <tr>
        <td>
            <?php 

            $this->widget('widgets.ImageViewWidget', array(
               'model' => $model,
               'behaviorName' => 'preview',
            ));

            ?>           
        </td>
        <td>
            <h1><?php echo $model->firstName." ".$model->middleName." ".$model->lastName; ?></h1>            
        </td>
    </tr>
    <tr>
        <td width="200px">
            <?php 
            $this->widget('bootstrap.widgets.TbBox', array(
                'title' => 'Perfil',
                'headerIcon' => 'icon-user',
                'content' => $this->renderPartial('_view_profile',array('model'=>$model),true)
            ));
            ?>
            <?php foreach ($model->professors as $professor){
            $this->widget('bootstrap.widgets.TbBox', array(
                'title' => 'Professor',
                'headerIcon' => 'icon-pencil',
                'content' => $this->renderPartial('_view_professor',array('model'=>$professor),true)
            ));         
            } ?>
            <?php foreach ($model->students as $student){
            $this->widget('bootstrap.widgets.TbBox', array(
                'title' => 'Estudante',
                'headerIcon' => 'icon-book',
                'content' => $this->renderPartial('_view_student',array('model'=>$student),true)
            ));         
            } ?>
            <?php foreach ($model->employees as $employee){
            $this->widget('bootstrap.widgets.TbBox', array(
                'title' => 'Funcionário',
                'headerIcon' => 'icon-envelope',
                'content' => $this->renderPartial('_view_employee',array('model'=>$employee),true)
            ));         
            } ?>              
        </td>
        <td valign="top">
            <?php 
            $this->widget('bootstrap.widgets.TbBox', array(
                'title' => 'Carreira',
                'headerIcon' => 'icon-briefcase',
                'content' => $this->renderPartial('_view_career',array('model'=>$model),true)
            ));         
            ?>
            <?php 
            $this->widget('bootstrap.widgets.TbBox', array(
                'title' => 'Observações',
                'headerIcon' => 'icon-list-alt',
                'content' => $this->renderPartial('_view_observation',array('model'=>$model),true)
            ));         
            ?> 
            
           
            
            <?php 
                $testimonials = Testimonials::model()->findAll('idPerson =' .$model->id . ' and isVisible = 1');

                foreach ($testimonials as $test) {
                    $del = '';
                    if (!Yii::app()->user->isGuest){
                        $del = '<br><br>' . CHtml::link('Deletar',Yii::app()->createUrl ("personDraft/deleteTestimonials",array('id'=>$test->id)));
                                
                    };
                    $this->widget('bootstrap.widgets.TbBox', array(
                        'title' => 'Depoimento enviado por ' . $test->name,
                        'content' => $test->text . '<br><div style="float:right;font-size:12px;color:gray">' .$test->date . '</div>' . $del,
                    )); 

                }

                ?>
            
             <?php
                $this->widget(
                    'bootstrap.widgets.TbButton',
                    array(
                        'label' => 'Enviar depoimento',
                        'type' => 'primary',
                        'htmlOptions' => array('class'=>'pull-right'),
                        'url' => $this->createUrl('Testimonials/create',array('id'=>$model->id)),
                    )
                );
             ?>

        </td>        
    </tr>
</table>


