<h3>Professor</h3>
<?php echo $form->toggleButtonRow($professor, 'isValid') ?>
<?php echo $form->datepickerRow($professor, 'enterDate',
array('hint'=>'Clique dentro para abrir o calendário',
'prepend'=>'<i class="icon-calendar"></i>',
'options'=>array('format'=>'dd/mm/yyyy'))); ?>
<?php echo $form->datepickerRow($professor, 'exitDate',
array('hint'=>'Clique dentro para abrir o calendário',
'prepend'=>'<i class="icon-calendar"></i>',
'options'=>array('format'=>'dd/mm/yyyy'))); ?>    
<?php echo $form->textFieldRow($professor, 'academicFormation'); ?>
<?php echo $form->textFieldRow($professor, 'researchArea'); ?>
<?php echo $form->textFieldRow($professor, 'department'); ?>
<?php echo $form->textFieldRow($professor, 'registry'); ?>