<?php
/* @var $this PersonController */
/* @var $model Employee */
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'enterDate',
		'exitDate',
		'academicFormation',
		'department',
                'job',
		'registry',
    ),
));
?>
