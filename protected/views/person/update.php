<?php
/* @var $this PersonController */
/* @var $model Person 
 * @var $student Student
 * @var $employee Employee
 * @var $professor Professor */

$this->breadcrumbs=array(
	'Pessoas'=>array('index'),
	$model->firstName=>array('view','id'=>$model->id),
	'Editar',
);

if (!Yii::app()->user->isGuest){
$this->menu=array(
	array('label'=>'Adicionar Pessoa', 'url'=>array('create')),
	array('label'=>'Ver informações', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gerenciar Pessoas', 'url'=>array('admin')),
);
}
?>

<h1>Editar <?php echo $model->firstName; ?></h1>

<?php $this->renderPartial('_newform', array(
                                            'model'=>$model,
                                            'student' => $student,
                                            'professor' => $professor,
                                            'employee' => $employee)
        ); ?>