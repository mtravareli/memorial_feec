<h3>Funcionário</h3>
<?php echo $form->toggleButtonRow($employee, 'isValid') ?>
<?php echo $form->datepickerRow($employee, 'enterDate',
array('hint'=>'Clique dentro para abrir o calendário',
'prepend'=>'<i class="icon-calendar"></i>',
'options'=>array('format'=>'dd/mm/yyyy'))); ?>
<?php echo $form->datepickerRow($employee, 'exitDate',
array('hint'=>'Clique dentro para abrir o calendário',
'prepend'=>'<i class="icon-calendar"></i>',
'options'=>array('format'=>'dd/mm/yyyy'))); ?>    
<?php echo $form->textFieldRow($employee, 'academicFormation'); ?>
<?php echo $form->textFieldRow($employee, 'department'); ?>
<?php echo $form->textFieldRow($employee, 'job'); ?>
<?php echo $form->textFieldRow($employee, 'registry'); ?>