<?php
/* @var $this PersonController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Contribuições',
);

?>

<h1>Contribuições para aprovação</h1>
<h2>Pessoas</h2>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block'=>true, // display a larger alert block?
    'fade'=>true, // use transitions?
    'closeText'=>'×', // close link text - if set to false, no close link is displayed
    'alerts'=>array( // configurations per alert type
	    'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
        ),
)); 
?>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */

$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
        'emptyText'=>"Não há pessoas que satisfaçam essa busca.",
        'summaryText'=>'Total: {count}',
	//'template'=>"{items}",
	'filter'=>$model,
	'columns'=>array(
            
              array(
                    'name'=>'name_search',
                    'header' => 'Nome',
                    'value'=>'$data->firstName . " " . $data->middleName . " " . $data->lastName', 
                ),
            
		//'id',
		//'firstName',
		//'middleName',
		//'lastName',
                'contact',
                //'is_student',
            
      
            
                array(
                    'name'=>'is_student',
                    'header' => 'Aluno',
                    'value'=>'isset(StudentDraft::Model()->findByAttributes(array("idPerson"=>$data->id))->class) ? "sim" : "não"', 
                    //// causes 'Trying to get property of non-object' - no error if $data->city
                     'filter'=>CHtml::activeDropDownList($model, 'is_student', array('2' => 'todos','0' => 'não', '1' => 'sim')),
                    //'filter'=>CHtml::activeDropDownList
                    //'value'=>'CHtml::checkBox("is_student",null,array("value"=>$data->id,"id"=>"cid_".$data->id))',
                    ),
            
                array(
                    'name'=>'is_employee',
                    'header' => 'Funcionário',
                    'value'=>'isset(EmployeeDraft::Model()->findByAttributes(array("idPerson"=>$data->id))->job) ? "sim" : "não"', 
                    //// causes 'Trying to get property of non-object' - no error if $data->city
                     'filter'=>CHtml::activeDropDownList($model, 'is_employee', array('2' => 'todos','0' => 'não', '1' => 'sim')),
                    ),
            
               array(
                    'name'=>'is_professor',
                    'header' => 'Professor',
                    'value'=>'isset(ProfessorDraft::Model()->findByAttributes(array("idPerson"=>$data->id))->registry) ? "sim" : "não"', 
                    //// causes 'Trying to get property of non-object' - no error if $data->city
                     'filter'=>CHtml::activeDropDownList($model, 'is_professor', array('2' => 'todos','0' => 'não', '1' => 'sim')),
                    ), 
                 
                array(
                       'header' => 'Confirmar dados',
                       'class'=>'CButtonColumn',
                       'template'=>'{view}',
                       'buttons'=>array(
                           'view'=>array(
                               'url'=>'Yii::app()->createUrl("person/createfromdraft",array("id"=>$data->id))',
                           )
                       )
                   ),
                array(
                       'header' => 'Rejeitar',
                       'class'=>'CButtonColumn',
                       'template'=>'{delete}',
                       'buttons'=>array(
                           'view'=>array(
                               'url'=>'Yii::app()->createUrl("personDraft/delete",array("id"=>$data->id))',
                           )
                       )
                   ),
            ),
    
));


?>
<br>
<br>
<h2>Depoimentos</h2>
<br>

<?php 
    $testimonials = Testimonials::model()->findAll('isVisible = 0');

    foreach ($testimonials as $test) {
        $person = Person::model()->find('id = ' . $test->idPerson );
        $this->widget('bootstrap.widgets.TbBox', array(
            'title' => 'Depoimento enviado por  "' . $test->name . '"  para ' . 
            CHtml::link($person->firstName . ' ' . $person->lastName,Yii::app()->createUrl ("/person/view",array("id"=>$data->id))) .
                ' em ' .$test->date . ' <div style="position: absolute;right: -350px;top: 0;">'.
            CHtml::link('Aceitar',Yii::app()->createUrl ("personDraft/AcceptTestimonials",array("id"=>$test->id)), array("style"=>"color:green")) . '' .
            '  ou  '.
            CHtml::link('Deletar',Yii::app()->createUrl ("personDraft/deleteTestimonials",array("id"=>$test->id)), array("style"=>"color:red")) . '</div>',
            'headerIcon' => 'icon-list-alt',
            'content' => $test->text,
        )); 

    }
?>