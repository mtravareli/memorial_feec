<?php
/* @var $this PersonController */
/* @var $model Person 
 * @var $student Student
 * @var $employee Employee
 * @var $professor Professor
 */

$this->breadcrumbs=array(
	'Contribuições'=>array('site/contributions'),
	'Pessoa',
);

?>

<h1>Enviar contribuição de Pessoa</h1>


<?php $this->renderPartial('_newform', array('model'=>$model,'student'=>$student, 'professor'=>$professor, 'employee'=>$employee)); ?>