<?php

class PersonDraftController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create', 'captcha'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','deleteTestimonials','acceptTestimonials'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PersonDraft;
                $student = new StudentDraft;
                $employee = new EmployeeDraft;
                $professor = new ProfessorDraft;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                $return = false;
                
                if(isset($_POST['PersonDraft'])) {
                    $model->attributes=$_POST['PersonDraft'];
                    $professor->attributes=$_POST['ProfessorDraft'];
                    $student->attributes=$_POST['StudentDraft'];
                    $employee->attributes=$_POST['EmployeeDraft'];
                    $student->isValid = $_POST['StudentDraft']['isValid'];
                    $professor->isValid = $_POST['ProfessorDraft']['isValid'];
                    $employee->isValid = $_POST['EmployeeDraft']['isValid'];
                    
                    if ($model->validate()) {
                            
                        if (strcmp($student->isValid,'1')==0) {
                            if($student->validate()) { 
                                $saveable['0']= $student; 
                                $model->is_student = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error1', '<strong>Erro nas informações de Aluno!</strong> Confira os dados específicos.');

                            }
                        }
                        if (strcmp($professor->isValid,'1')==0) {
                            if($professor->validate()) { 
                                $saveable['1']= $professor;  
                                $model->is_professor = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error2', '<strong>Erro nas informações de Professor!</strong> Confira os dados específicos.');
                            }
                        }
                        if (strcmp($employee->isValid,'1')==0) {
                            if($employee->validate()) { 
                                $saveable['2']= $employee; 
                                $model->is_employee = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error3', '<strong>Erro nas informações de Funcionário!</strong> Confira os dados específicos.');
                            }
                        }
                        
                        if (isset($saveable)) {
                            if ($return == false) {
                                $connection=Yii::app()->db;
                                $transaction=$connection->beginTransaction();
                                try{
                                    $model->save();

                                    if (isset($saveable['0'])){
                                        $student->idPerson = $model->id;
                                        $student->save();
                                    }
                                    if (isset($saveable['1'])){
                                        $professor->idPerson = $model->id;
                                        $professor->save();
                                    }
                                    if (isset($saveable['2'])){
                                        $employee->idPerson = $model->id;
                                        $employee->save();
                                    }
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', '<strong>Contribuição enviada com sucesso! Muito Obrigado.</strong>');
                                }catch(Exception $e) // an exception is raised if a query fails
                                {
                                    $transaction->rollback();
                                }
                                
                                $this->redirect(array('site/contributions'));
                            } 
                        } else {
                                if ($return == false) Yii::app()->user->setFlash('error', '<strong>Erro!</strong> Você precisa habilitar pelo menos um: aluno, funcionário ou professor.');
                            }
                            
                    }
    
                }
		$this->render('create',array(
			'model'=>$model,
                        'student' => $student,
                        'professor' => $professor,
                        'employee' => $employee,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                if (isset($model->students[0])) {
                    $student = $model->students[0];
                    $student->isValid = 1;
                } else {
                    $student = new StudentDraft();
                }
                               
                if (isset($model->professors[0])) {
                    $professor = $model->professors[0];
                    $professor->isValid = 1;
                } else {
                    $professor = new ProfessorDraftDraft();
                }
                
                if (isset($model->employees[0])) {
                    $employee = $model->employees[0];
                    $employee->isValid = 1;
                } else {
                    $employee = new EmployeeDraft();
                }
                

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$return = false;
                
                if(isset($_POST['PersonDraft'])) {
                    $model->attributes=$_POST['PersonDraft'];
                    $professor->attributes=$_POST['ProfessorDraft'];
                    $student->attributes=$_POST['StudentDraft'];
                    $employee->attributes=$_POST['EmployeeDraft'];
                    $student->isValid = $_POST['StudentDraft']['isValid'];
                    $professor->isValid = $_POST['ProfessorDraft']['isValid'];
                    $employee->isValid = $_POST['EmployeeDraft']['isValid'];
                    
                    if ($model->validate()) {
                            
                        if (strcmp($student->isValid,'1')==0) {
                            if($student->validate()) { 
                                $saveable['0']= $student;  
                                $model->is_student = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error1', '<strong>Erro nas informações de Aluno!</strong> Confira os dados específicos.');
                            }
                        } else {
                             if (isset($model->students[0])) {
                                 $model->students[0]->delete();
                                 $model->is_student = false;
                             }
                         }
                         
                        if (strcmp($professor->isValid,'1')==0) {
                            if($professor->validate()) { 
                                $saveable['1']= $professor;  
                                $model->is_professor = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error2', '<strong>Erro nas informações de Professor!</strong> Confira os dados específicos.');
                            }
                        } else {
                             if (isset($model->professors[0])) {
                                 $model->professors[0]->delete();
                                 $model->is_professor = false;
                             }
                         }
                         
                        if (strcmp($employee->isValid,'1')==0) {
                            if($employee->validate()) { 
                                $saveable['2']= $employee;  
                                $model->is_employee = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error3', '<strong>Erro nas informações de Funcionário!</strong> Confira os dados específicos.');
                            }
                        } else {
                             if (isset($model->employees[0])) {
                                 $model->employees[0]->delete();
                                 $model->is_employee = false;
                             }
                         }
                        
                        if (isset($saveable)) {
                            if ($return == false) {
                                $connection=Yii::app()->db;
                                $transaction=$connection->beginTransaction();
                                try{
                                    $model->save();

                                    if (isset($saveable['0'])){
                                        $student->idPerson = $model->id;
                                        $student->save();
                                    }
                                    if (isset($saveable['1'])){
                                        $professor->idPerson = $model->id;
                                        $professor->save();
                                    }
                                    if (isset($saveable['2'])){
                                        $employee->idPerson = $model->id;
                                        $employee->save();
                                    }
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', '<strong>Contribuição enviada com sucesso! Muito Obrigado.</strong>');
                                }catch(Exception $e) // an exception is raised if a query fails
                                {
                                    $transaction->rollback();
                                }
                                
                                $this->redirect(array('view','id'=>$model->id));
                            } 
                        } else {
                                if ($return == false) Yii::app()->user->setFlash('error', '<strong>Erro!</strong> Você precisa habilitar pelo menos um: aluno, funcionário ou professor.');
                            }
                            
                    }
    
                }
		$this->render('update',array(
			'model'=>$model,
                        'student' => $student,
                        'professor' => $professor,
                        'employee' => $employee,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$persondraft = $this->loadModel($id);
                

                
                $connection=Yii::app()->db;
                $transaction=$connection->beginTransaction();
                try {
                    if ($persondraft->is_student) {
                        $studentdraft = StudentDraft::model()->find('idPerson = ' .$id); 
                        $studentdraft->delete();
                    }
                    if ($persondraft->is_professor) {
                        $professordraft = ProfessorDraft::model()->find('idPerson = ' .$id); 
                        $professordraft->delete();
                    }
                    if ($persondraft->is_employee) {
                        $employeedraft = EmployeeDraft::model()->find('idPerson = ' .$id); 
                        $employeedraft->delete();
                    }
                    $persondraft->delete();
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', '<strong>Pessoa deletada com sucesso!</strong>');
                }catch(Exception $e) // an exception is raised if a query fails
                {
                    $transaction->rollback();
                }
                

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
                $model=new PersonDraft('search');
                $model->unsetAttributes();  // clear any default values
		if(isset($_GET['PersonDraft']))
			$model->attributes=$_GET['PersonDraft'];
                
		$dataProvider=new CActiveDataProvider('PersonDraft');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
                        'model' => $model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PersonDraft('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PersonDraft']))
			$model->attributes=$_GET['PersonDraft'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionDeleteTestimonials($id)
	{
            Testimonials::model()->find('id = ' .$id)->delete(); 
            Yii::app()->user->setFlash('success', 'Depoimento deletado com sucesso.');
                 
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}
        
        public function actionAcceptTestimonials($id)
	{
            $test = Testimonials::model()->find('id = ' .$id); 
            $test->isvisible = 1;
            $test->save();
            Yii::app()->user->setFlash('success', 'Depoimento aprovado com sucesso.');
                 
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PersonDraft the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PersonDraft::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}        
        
}
