<?php

class PersonController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','createfromdraft','admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                if (Yii::app()->user->isGuest){
                $this->layout='//layouts/column1';
                }
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
                if (Yii::app()->user->isGuest){
                $this->layout='//layouts/column2';
                }
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Person;
                $student = new Student;
                $employee = new Employee;
                $professor = new Professor;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                $return = false;
                
                if(isset($_POST['Person'])) {
                    $model->attributes=$_POST['Person'];
                    $professor->attributes=$_POST['Professor'];
                    $student->attributes=$_POST['Student'];
                    $employee->attributes=$_POST['Employee'];
                    $student->isValid = $_POST['Student']['isValid'];
                    $professor->isValid = $_POST['Professor']['isValid'];
                    $employee->isValid = $_POST['Employee']['isValid'];
                    
                    if ($model->validate()) {
                            
                        if (strcmp($student->isValid,'1')==0) {
                            if($student->validate()) { 
                                $saveable['0']= $student; 
                                $model->is_student = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error1', '<strong>Erro nas informações de Aluno!</strong> Confira os dados específicos.');

                            }
                        }
                        if (strcmp($professor->isValid,'1')==0) {
                            if($professor->validate()) { 
                                $saveable['1']= $professor;  
                                $model->is_professor = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error2', '<strong>Erro nas informações de Professor!</strong> Confira os dados específicos.');
                            }
                        }
                        if (strcmp($employee->isValid,'1')==0) {
                            if($employee->validate()) { 
                                $saveable['2']= $employee; 
                                $model->is_employee = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error3', '<strong>Erro nas informações de Funcionário!</strong> Confira os dados específicos.');
                            }
                        }
                        
                        if (isset($saveable)) {
                            if ($return == false) {
                                $connection=Yii::app()->db;
                                $transaction=$connection->beginTransaction();
                                try{
                                    $model->save();

                                    if (isset($saveable['0'])){
                                        $student->idPerson = $model->id;
                                        $student->save();
                                    }
                                    if (isset($saveable['1'])){
                                        $professor->idPerson = $model->id;
                                        $professor->save();
                                    }
                                    if (isset($saveable['2'])){
                                        $employee->idPerson = $model->id;
                                        $employee->save();
                                    }
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', '<strong>Pessoa salva com sucesso!</strong>');
                                }catch(Exception $e) // an exception is raised if a query fails
                                {
                                    $transaction->rollback();
                                }
                                
                                $this->redirect(array('view','id'=>$model->id));
                            } 
                        } else {
                                if ($return == false) Yii::app()->user->setFlash('error', '<strong>Erro!</strong> Você precisa habilitar pelo menos um: aluno, funcionário ou professor.');
                            }
                            
                    }
    
                }
		$this->render('create',array(
			'model'=>$model,
                        'student' => $student,
                        'professor' => $professor,
                        'employee' => $employee,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                if (isset($model->students[0])) {
                    $student = $model->students[0];
                    $student->isValid = 1;
                } else {
                    $student = new Student();
                }
                               
                if (isset($model->professors[0])) {
                    $professor = $model->professors[0];
                    $professor->isValid = 1;
                } else {
                    $professor = new Professor();
                }
                
                if (isset($model->employees[0])) {
                    $employee = $model->employees[0];
                    $employee->isValid = 1;
                } else {
                    $employee = new Employee();
                }
                

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$return = false;
                
                if(isset($_POST['Person'])) {
                    $model->attributes=$_POST['Person'];
                    $professor->attributes=$_POST['Professor'];
                    $student->attributes=$_POST['Student'];
                    $employee->attributes=$_POST['Employee'];
                    $student->isValid = $_POST['Student']['isValid'];
                    $professor->isValid = $_POST['Professor']['isValid'];
                    $employee->isValid = $_POST['Employee']['isValid'];
                    
                    if ($model->validate()) {
                            
                        if (strcmp($student->isValid,'1')==0) {
                            if($student->validate()) { 
                                $saveable['0']= $student;  
                                $model->is_student = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error1', '<strong>Erro nas informações de Aluno!</strong> Confira os dados específicos.');
                            }
                        } else {
                             if (isset($model->students[0])) {
                                 $model->students[0]->delete();
                                 $model->is_student = false;
                             }
                         }
                         
                        if (strcmp($professor->isValid,'1')==0) {
                            if($professor->validate()) { 
                                $saveable['1']= $professor;  
                                $model->is_professor = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error2', '<strong>Erro nas informações de Professor!</strong> Confira os dados específicos.');
                            }
                        } else {
                             if (isset($model->professors[0])) {
                                 $model->professors[0]->delete();
                                 $model->is_professor = false;
                             }
                         }
                         
                        if (strcmp($employee->isValid,'1')==0) {
                            if($employee->validate()) { 
                                $saveable['2']= $employee;  
                                $model->is_employee = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error3', '<strong>Erro nas informações de Funcionário!</strong> Confira os dados específicos.');
                            }
                        } else {
                             if (isset($model->employees[0])) {
                                 $model->employees[0]->delete();
                                 $model->is_employee = false;
                             }
                         }
                        
                        if (isset($saveable)) {
                            if ($return == false) {
                                $connection=Yii::app()->db;
                                $transaction=$connection->beginTransaction();
                                try{
                                    $model->save();

                                    if (isset($saveable['0'])){
                                        $student->idPerson = $model->id;
                                        $student->save();
                                    }
                                    if (isset($saveable['1'])){
                                        $professor->idPerson = $model->id;
                                        $professor->save();
                                    }
                                    if (isset($saveable['2'])){
                                        $employee->idPerson = $model->id;
                                        $employee->save();
                                    }
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', '<strong>Pessoa salva com sucesso!</strong>');
                                }catch(Exception $e) // an exception is raised if a query fails
                                {
                                    $transaction->rollback();
                                }
                                
                                $this->redirect(array('view','id'=>$model->id));
                            } 
                        } else {
                                if ($return == false) Yii::app()->user->setFlash('error', '<strong>Erro!</strong> Você precisa habilitar pelo menos um: aluno, funcionário ou professor.');
                            }
                            
                    }
    
                }
		$this->render('update',array(
			'model'=>$model,
                        'student' => $student,
                        'professor' => $professor,
                        'employee' => $employee,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$person = $this->loadModel($id);
                
                $connection=Yii::app()->db;
                $transaction=$connection->beginTransaction();
                try {
                    if (isset($person->students[0])) {
                        $person->students[0]->delete();
                    }
                    if (isset($person->professors[0])) {
                        $person->professors[0]->delete();
                    }
                    if (isset($person->employees[0])) {
                        $person->employees[0]->delete();
                    }
                    $person->delete();
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', '<strong>Pessoa deletada com sucesso!</strong>');
                }catch(Exception $e) // an exception is raised if a query fails
                {
                    $transaction->rollback();
                }
                

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
                $this->layout='//layouts/column1';
                $model=new Person('search');
                $model->unsetAttributes();  // clear any default values
		if(isset($_GET['Person']))
			$model->attributes=$_GET['Person'];
                
		$dataProvider=new CActiveDataProvider('Person');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
                        'model' => $model,
		));
                $this->layout='//layouts/column2';
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Person('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Person']))
			$model->attributes=$_GET['Person'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionCreatefromdraft($id) {
            Yii::import('application.controllers.PersonDraftController');
            if(isset($_GET['id'])) {
                $id = $_GET['id'];
                $persondraft = PersonDraftController::loadModel($id);
                
                
                
                $model = new Person;
                $model->firstName = $persondraft->firstName;
                $model->middleName = $persondraft->middleName;
                $model->lastName = $persondraft->lastName;
                $model->birthDate = $persondraft->birthDate;
                $model->deathDate = $persondraft->deathDate;
                $model->career = $persondraft->career;
                $model->observations = $persondraft->observations;
                $model->contact = $persondraft->contact;
                $model->is_student = $persondraft->is_student;
                $model->is_employee = $persondraft->is_employee;
                $model->is_professor = $persondraft->is_professor;
                
                $professor = new Professor;
                $student = new Student;
                $employee = new Employee;
                
                
                if ($persondraft->is_professor) {
                    $professordraft = ProfessorDraft::model()->find('idPerson = ' .$id); 
                    $professor->attributes = $professordraft->attributes ;
                    $professor->isValid = 1;
                }
                  
                if ($persondraft->is_student) {
                    $studentdraft = StudentDraft::model()->find('idPerson = ' .$id); 
                    $student->attributes = $studentdraft->attributes ;
                    $student->isValid = 1;
                } 
                
                if ($persondraft->is_employee) {
                    $employeedraft = EmployeeDraft::model()->find('idPerson = ' .$id);    
                    $employee->attributes = $employeedraft->attributes ;
                    $employee->isValid = 1;
                } 
                
            } else {
            
                $model=new Person;     
                $student = new Student;
                $employee = new Employee;
                $professor = new Professor;
            }
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                $return = false;
                
                if(isset($_POST['Person'])) {
                    $model->attributes=$_POST['Person'];
                    $professor->attributes=$_POST['Professor'];
                    $student->attributes=$_POST['Student'];
                    $employee->attributes=$_POST['Employee'];
                    $student->isValid = $_POST['Student']['isValid'];
                    $professor->isValid = $_POST['Professor']['isValid'];
                    $employee->isValid = $_POST['Employee']['isValid'];
                    
                    if ($model->validate()) {
                            
                        if (strcmp($student->isValid,'1')==0) {
                            if($student->validate()) { 
                                $saveable['0']= $student; 
                                $model->is_student = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error1', '<strong>Erro nas informações de Aluno!</strong> Confira os dados específicos.');

                            }
                        }
                        if (strcmp($professor->isValid,'1')==0) {
                            if($professor->validate()) { 
                                $saveable['1']= $professor;  
                                $model->is_professor = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error2', '<strong>Erro nas informações de Professor!</strong> Confira os dados específicos.');
                            }
                        }
                        if (strcmp($employee->isValid,'1')==0) {
                            if($employee->validate()) { 
                                $saveable['2']= $employee; 
                                $model->is_employee = true;
                            } else {
                                $return = true;
                                Yii::app()->user->setFlash('error3', '<strong>Erro nas informações de Funcionário!</strong> Confira os dados específicos.');
                            }
                        }
                        
                        if (isset($saveable)) {
                            if ($return == false) {
                                $connection=Yii::app()->db;
                                $transaction=$connection->beginTransaction();
                                try{
                                    $model->save();

                                    if (isset($saveable['0'])){
                                        $student->idPerson = $model->id;
                                        $student->save();
                                    }
                                    if (isset($saveable['1'])){
                                        $professor->idPerson = $model->id;
                                        $professor->save();
                                    }
                                    if (isset($saveable['2'])){
                                        $employee->idPerson = $model->id;
                                        $employee->save();
                                    }
                                    //$transaction->commit();
                                    Yii::app()->user->setFlash('success', '<strong>Pessoa salva com sucesso!</strong>');
                                    
                                            //delete draft of db
                                            if ($persondraft->is_student) {
                                                $studentdraft->delete();
                                            }
                                            if ($persondraft->is_professor) {
                                                $professordraft->delete();
                                            }
                                            if ($persondraft->is_employee) {
                                                $employeedraft->delete();
                                            }
                                          
                                           
                                            $persondraft->delete();
                                            $transaction->commit();
                                    
                                    
                                    
                                }catch(Exception $e) // an exception is raised if a query fails
                                {
                                    $transaction->rollback();
                                }
                                
                                $this->redirect(array('view','id'=>$model->id));
                            } 
                        } else {
                                if ($return == false) Yii::app()->user->setFlash('error', '<strong>Erro!</strong> Você precisa habilitar pelo menos um: aluno, funcionário ou professor.');
                            }
                            
                    }
    
                }
		$this->render('create',array(
			'model'=>$model,
                        'student' => $student,
                        'professor' => $professor,
                        'employee' => $employee,
		));
                
                
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Person the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Person::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Person $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='person-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
}
