<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
                        'saveImageAttachment' => 'ext.imageAttachment.ImageAttachmentAction',
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
            
                if(isset($_POST['startDate'])) {
			$startDate=$_POST['startDate']; 
                } else {
                        $startDate = 1970;
                }
                
                if(isset($_POST['endDate'])) {
			$endDate=$_POST['endDate']; 
                } else {
                        $endDate = 2014;
                }

                $timeline = array();
                $people=Person::model()->limit_with_priority()->findAll();
                foreach ($people as $person) {

                    //FALECIMENTO DE PESSOAS
                    if ((strtotime(str_replace('/', '-', $person->deathDate)) >= strtotime("01-01-".$startDate)) && (strtotime(str_replace('/', '-', $person->deathDate)) <= strtotime("31-12-".$endDate))) {
                        $timeline[] = array('person' => $person, 'event' => 'death:Person', 'date' => $person->deathDate);
                    }

                    //ENTRADA E SAIDA DE PESSOAS NA FACULDADE
                    if (isset($person->firstProfessor)) {
                        if ((strtotime(str_replace('/', '-', $person->firstProfessor->enterDate)) >= strtotime("01-01-".$startDate)) && (strtotime(str_replace('/', '-', $person->firstProfessor->enterDate)) <= strtotime("31-12-".$endDate))) {
                            $timeline[] = array('person' => $person, 'event' => 'in:Professor', 'date' => $person->firstProfessor->enterDate);
                        }
                        if ((strtotime(str_replace('/', '-', $person->firstProfessor->exitDate)) >= strtotime("01-01-".$startDate)) && (strtotime(str_replace('/', '-', $person->firstProfessor->exitDate)) <= strtotime("31-12-".$endDate))) {
                            $timeline[] = array('person' => $person, 'event' => 'out:Professor', 'date' => $person->firstProfessor->exitDate);
                        }
                    }
                    if (isset($person->firstEmployee)) {
                        if ((strtotime(str_replace('/', '-', $person->firstEmployee->enterDate)) >= strtotime("01-01-".$startDate)) && (strtotime(str_replace('/', '-', $person->firstEmployee->enterDate)) <= strtotime("31-12-".$endDate))) {
                            $timeline[] = array('person' => $person, 'event' => 'in:Employee','date' => $person->firstEmployee->enterDate);
                        }
                        if ((strtotime(str_replace('/', '-', $person->firstEmployee->exitDate)) >= strtotime("01-01-".$startDate)) && (strtotime(str_replace('/', '-', $person->firstEmployee->exitDate)) <= strtotime("31-12-".$endDate))) {
                            $timeline[] = array('person' => $person, 'event' => 'out:Employee','date' => $person->firstEmployee->exitDate);
                        }
                    }
                    if (isset($person->firstStudent)) {
                        if ((strtotime(str_replace('/', '-', $person->firstStudent->enterDate)) >= strtotime("01-01-".$startDate)) && (strtotime(str_replace('/', '-', $person->firstStudent->enterDate)) <= strtotime("31-12-".$endDate))) {
                            $timeline[] = array('person' => $person, 'event' => 'in:Student','date' => $person->firstStudent->enterDate);
                        }
                        if ((strtotime(str_replace('/', '-', $person->firstStudent->exitDate)) >= strtotime("01-01-".$startDate)) && (strtotime(str_replace('/', '-', $person->firstStudent->exitDate)) <= strtotime("31-12-".$endDate))) {
                            $timeline[] = array('person' => $person, 'event' => 'out:Student','date' => $person->firstStudent->exitDate);
                        }
                    }
                    
                    

                }
                
                if (count($timeline) < 50) {
                     foreach ($people as $person) {
                         $testimonials = Testimonials::model()->findAll('idPerson = ' . $person->id .' and isVisible = 1');
                         if($testimonials !== null){
                            foreach ($testimonials as $test) {
                                $timeline[] = array('testimonial' => $test, 'event' => "testimonial:$person->firstName $person->lastName",'date' => $test->date);
                            }
                         }
                     }
                }
                
                //NUMERO MAXIMO DE EVENTOS NA TIMELINE = 50
                if (count($timeline) > 50) {
                    $timeline = array_slice($timeline, 0, 50); 
                }
                
                
                //funcao aux para o sort para ordenar por data
                function cmp($a, $b)
                {
                           
                    if (strtotime(str_replace('/', '-', $a['date'])) >= strtotime(str_replace('/', '-', $b['date']))) {
                        return -1;
                    } else {
                        return +1;
                    }
                }

                usort($timeline, "cmp");

               
                
		//$dataProvider=new CActiveDataProvider('Person');
		$this->render('index',array(
			//'dataProvider'=>$dataProvider,
                        'timeline' => $timeline,
                        'startDate' => $startDate,
                        'endDate' => $endDate,
		));
                
		
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Obrigado por entrar em contato. Nós lhe responderemos assim que possível.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
        
	/**
	 * Displays the search.
	 */
	public function actionSearch()
	{
                if(isset($_POST['query'])){
                    $query = $_POST['query'];
                } else {
                    $query = "";
                }
                
                $model=new Person('searchGeneral');
                $model->unsetAttributes();  // clear any default values
                $model->firstName = $query;
                $model->middleName = $query;
                $model->lastName = $query;
                $model->name_search = $query;
                $model->birthDate = $query;
                $model->deathDate = $query;
                $model->deathDate = $query;
                $model->contact = $query;
                $model->career = $query;
                $model->observations = $query;  
                
                $employee=new Employee();
                $employee->enterDate = $query;
                $employee->exitDate = $query;
                $employee->academicFormation = $query;
                $employee->department = $query;
                $employee->job = $query;
                $employee->registry = $query;                
                $model->firstEmployee = $employee;

                $professor=new Professor();
                $professor->enterDate = $query;
                $professor->exitDate = $query;
                $professor->academicFormation = $query;
                $professor->researchArea = $query;
                $professor->department = $query;
                $professor->registry = $query;                
                $model->firstProfessor = $professor;
                
                $student=new Student();
                $student->enterDate = $query;
                $student->exitDate = $query;
                $student->nickname = $query;
                $student->course = $query;
                $student->class = $query;
                $student->academicEntities = $query;
                $student->ra = $query;  
                $model->firstStudent = $student;  
                
		$this->render('search',array('queryText'=>$query, 'model' => $model,));
	}
        
	public function actionContributions()
	{
                 $this->render('contributions');
	}        
        
}