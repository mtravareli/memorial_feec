<?php

/**
 * This is the model class for table "person_draft".
 *
 * The followings are the available columns in table 'person_draft':
 * @property integer $id
 * @property string $firstName
 * @property string $middleName
 * @property string $lastName
 * @property string $birthDate
 * @property string $deathDate
 * @property string $contact
 * @property string $career
 * @property string $observations
 * @property integer $priority
 * @property boolean $is_student
 * @property boolean $is_professor
 * @property boolean $is_employee
 *
 * The followings are the available model relations:
 * @property EmployeeDraft[] $employeeDrafts
 * @property ProfessorDraft[] $professorDrafts
 * @property StudentDraft[] $studentDrafts
 */
class PersonDraft extends CActiveRecord
{
        public $name_search;
        public $isStudent;
        public $isProfessor;
        public $isEmployee;
        
        public $verifyCode;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'person_draft';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstName, lastName', 'required'),
			array('priority', 'numerical', 'integerOnly'=>true),
			array('firstName, lastName', 'length', 'max'=>50),
			array('middleName, contact', 'length', 'max'=>100),
                        array('birthDate, deathDate','safe'),
			array('career, observations', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, firstName, middleName, lastName, birthDate, deathDate, contact, career, observations, priority, is_student,is_professor,is_employee, name_search, isStudent,isEmployee,isProfessor', 'safe', 'on'=>'search'),
                        // verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'employees' => array(self::HAS_MANY, 'EmployeeDraft', 'idPerson'),
			'professors' => array(self::HAS_MANY, 'ProfessorDraft', 'idPerson'),
			'students' => array(self::HAS_MANY, 'StudentDraft', 'idPerson'),
                        'firstEmployee' => array(self::HAS_ONE, 'EmployeeDraft', 'idPerson'), // the relation for first employee
                        'firstProfessor' => array(self::HAS_ONE, 'ProfessorDraft', 'idPerson'), // the relation for first professor
                        'firstStudent' => array(self::HAS_ONE, 'StudentDraft', 'idPerson'), // the relation for first student     
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'firstName' => 'Primeiro Nome',
			'middleName' => 'Nome do meio',
			'lastName' => 'Sobrenome',
			'birthDate' => 'Data de nascimento',
			'deathDate' => 'Data de falecimento',
			'contact' => 'Contato',
			'career' => 'Carreira',
			'observations' => 'Observações',
			'priority' => 'Prioridade',
                        'verifyCode'=>'Código de verificação',
                        
                        
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;
                
		$criteria->compare('id',$this->id);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('middleName',$this->middleName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('birthDate',$this->birthDate,true);
		$criteria->compare('deathDate',$this->deathDate,true);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('career',$this->career,true);
		$criteria->compare('observations',$this->observations,true);
		$criteria->compare('priority',$this->priority);
                
                $criteria->compare('firstName',$this->name_search,true,"OR");
                $criteria->compare('middleName',$this->name_search,true,"OR");
                $criteria->compare('lastName',$this->name_search,true,"OR");
                
                //echo $this->is_student;
                if ($this->is_student == '1' ) {
                    $criteria->compare('is_student',1);
                } else if ($this->is_student == '0' ) {
                    $criteria->compare('is_student',0);
                }
                
                if ($this->is_professor == '1') {
                    $criteria->compare('is_professor',1);
                } else if ($this->is_professor == '0' ) {
                    $criteria->compare('is_professor',0);
                }
                
                if ($this->is_employee == '1') {
                    $criteria->compare('is_employee',1);
                } else if ($this->is_employee == '0' ) {
                    $criteria->compare('is_employee',0);
                }
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array(
                            'attributes'=>array(
                                'name_search'=>array(
                                    'asc'=>'firstName',
                                    'desc'=>'firstName DESC',
                                ),
                                '*',
                            ),
                        ),
                        
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PersonDraft the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
