<?php

/**
 * This is the model class for table "student_draft".
 *
 * The followings are the available columns in table 'student_draft':
 * @property integer $id
 * @property string $enterDate
 * @property string $exitDate
 * @property string $nickname
 * @property string $course
 * @property string $class
 * @property string $academicEntities
 * @property integer $idPerson
 * @property integer $ra
 *
 * The followings are the available model relations:
 * @property PersonDraft $idPerson0
 */
class StudentDraft extends CActiveRecord
{
        public $isValid;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'student_draft';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('course, class, ra', 'required'),
			array('idPerson, ra', 'numerical', 'integerOnly'=>true),
			array('enterDate, exitDate', 'length', 'max'=>11),
			array('nickname', 'length', 'max'=>50),
			array('course', 'length', 'max'=>200),
			array('class', 'length', 'max'=>100),
			array('academicEntities', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, enterDate, exitDate, nickname, course, class, academicEntities, idPerson, ra', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPerson0' => array(self::BELONGS_TO, 'PersonDraft', 'idPerson'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'enterDate' => 'Data de início',
			'exitDate' => 'Data de término',
			'nickname' => 'Apelido',
			'course' => 'Curso',
			'class' => 'Classe',
			'academicEntities' => 'Entidades acadêmicas',
			'idPerson' => 'Id Person',
			'ra' => 'Registro acadêmico',
                        'isValid' => 'É aluno?'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('enterDate',$this->enterDate,true);
		$criteria->compare('exitDate',$this->exitDate,true);
		$criteria->compare('nickname',$this->nickname,true);
		$criteria->compare('course',$this->course,true);
		$criteria->compare('class',$this->class,true);
		$criteria->compare('academicEntities',$this->academicEntities,true);
		$criteria->compare('idPerson',$this->idPerson);
		$criteria->compare('ra',$this->ra);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentDraft the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
