<?php

/**
 * This is the model class for table "person".
 *
 * The followings are the available columns in table 'person':
 * @property integer $id
 * @property string $firstName
 * @property string $middleName
 * @property string $lastName
 * @property string $birthDate
 * @property string $deathDate
 * @property string $contact
 * @property string $career
 * @property string $observations
 * @property integer $priority
 * @property boolean $is_student
 * @property boolean $is_professor
 * @property boolean $is_employee
 *
 * The followings are the available model relations:
 * @property Employee[] $employees
 * @property Professor[] $professors
 * @property Student[] $students
 */
class Person extends CActiveRecord
{
        public $name_search;
        public $isStudent;
        public $isProfessor;
        public $isEmployee;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'person';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstName, lastName, priority', 'required'),
			array('priority', 'numerical', 'integerOnly'=>true),
			array('firstName, lastName', 'length', 'max'=>50),
			array('middleName, contact', 'length', 'max'=>100),
                        array('birthDate, deathDate','safe'),
			array('career, observations', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, firstName, middleName, lastName, birthDate, deathDate, contact, career, observations, priority, is_student,is_professor,is_employee, name_search, isStudent,isEmployee,isProfessor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'employees' => array(self::HAS_MANY, 'Employee', 'idPerson'),
			'professors' => array(self::HAS_MANY, 'Professor', 'idPerson'),
			'students' => array(self::HAS_MANY, 'Student', 'idPerson'),
                        'firstEmployee' => array(self::HAS_ONE, 'Employee', 'idPerson'), // the relation for first employee
                        'firstProfessor' => array(self::HAS_ONE, 'Professor', 'idPerson'), // the relation for first professor
                        'firstStudent' => array(self::HAS_ONE, 'Student', 'idPerson'), // the relation for first student     
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'firstName' => 'Primeiro Nome',
			'middleName' => 'Nome do meio',
			'lastName' => 'Sobrenome',
			'birthDate' => 'Data de nascimento',
			'deathDate' => 'Data de falecimento',
			'contact' => 'Contato',
			'career' => 'Carreira',
			'observations' => 'Observações',
			'priority' => 'Prioridade',
                        
                        
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;
                
		$criteria->compare('id',$this->id);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('middleName',$this->middleName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('birthDate',$this->birthDate,true);
		$criteria->compare('deathDate',$this->deathDate,true);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('career',$this->career,true);
		$criteria->compare('observations',$this->observations,true);
		$criteria->compare('priority',$this->priority);
                
                $criteria->compare('firstName',$this->name_search,true,"OR");
                $criteria->compare('middleName',$this->name_search,true,"OR");
                $criteria->compare('lastName',$this->name_search,true,"OR");
                
                //echo $this->is_student;
                if ($this->is_student == '1' ) {
                    $criteria->compare('is_student',1);
                } else if ($this->is_student == '0' ) {
                    $criteria->compare('is_student',0);
                }
                
                if ($this->is_professor == '1') {
                    $criteria->compare('is_professor',1);
                } else if ($this->is_professor == '0' ) {
                    $criteria->compare('is_professor',0);
                }
                
                if ($this->is_employee == '1') {
                    $criteria->compare('is_employee',1);
                } else if ($this->is_employee == '0' ) {
                    $criteria->compare('is_employee',0);
                }
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array(
                            'attributes'=>array(
                                'name_search'=>array(
                                    'asc'=>'firstName',
                                    'desc'=>'firstName DESC',
                                ),
                                '*',
                            ),
                        ),
                        
		));
	}

        
	public function searchGeneral()
	{

		$criteria=new CDbCriteria;
                
                $criteria->with = (array('firstEmployee', 'firstProfessor', 'firstStudent'));
		//$criteria->compare('id',$this->id);
		$criteria->compare('firstName',$this->firstName,true,"OR");
		$criteria->compare('middleName',$this->middleName,true,"OR");
		$criteria->compare('lastName',$this->lastName,true,"OR");
		$criteria->compare('birthDate',$this->birthDate,true,"OR");
		$criteria->compare('deathDate',$this->deathDate,true,"OR");
		$criteria->compare('contact',$this->contact,true,"OR");
		$criteria->compare('career',$this->career,true,"OR");
		$criteria->compare('observations',$this->observations,true,"OR");
                
                $criteria->compare('firstName',$this->name_search,true,"OR");
                $criteria->compare('middleName',$this->name_search,true,"OR");
                $criteria->compare('lastName',$this->name_search,true,"OR");                
                //$criteria->compare('priority',$this->priority);
                
                // Employee
                if(isset($this->firstEmployee)){
                     $criteria->compare('firstEmployee.enterDate',$this->firstEmployee->enterDate,true,"OR");
                     $criteria->compare('firstEmployee.exitDate',$this->firstEmployee->exitDate,true,"OR");
                     $criteria->compare('firstEmployee.academicFormation',$this->firstEmployee->academicFormation,true,"OR");
                     $criteria->compare('firstEmployee.department',$this->firstEmployee->department,true,"OR");
                     $criteria->compare('firstEmployee.job',$this->firstEmployee->job,true,"OR");
                     $criteria->compare('firstEmployee.registry',$this->firstEmployee->registry,true,"OR");                 
                }
                
                // Professor
                if(isset($this->firstProfessor)){
                     $criteria->compare('firstProfessor.enterDate',$this->firstProfessor->enterDate,true,"OR");
                     $criteria->compare('firstProfessor.exitDate',$this->firstProfessor->exitDate,true,"OR");
                     $criteria->compare('firstProfessor.academicFormation',$this->firstProfessor->academicFormation,true,"OR");
                     $criteria->compare('firstProfessor.researchArea',$this->firstProfessor->researchArea,true,"OR");
                     $criteria->compare('firstProfessor.department',$this->firstProfessor->department,true,"OR");
                     $criteria->compare('firstProfessor.registry',$this->firstProfessor->registry,true,"OR");                                      
                }
                
                // Student
                if(isset($this->firstStudent)){
                     $criteria->compare('firstStudent.enterDate',$this->firstStudent->enterDate,true,"OR");
                     $criteria->compare('firstStudent.exitDate',$this->firstStudent->exitDate,true,"OR");
                     $criteria->compare('firstStudent.nickname',$this->firstStudent->nickname,true,"OR");
                     $criteria->compare('firstStudent.course',$this->firstStudent->course,true,"OR");                     
                     $criteria->compare('firstStudent.class',$this->firstStudent->class,true,"OR");
                     $criteria->compare('firstStudent.academicEntities',$this->firstStudent->academicEntities,true,"OR");
                     $criteria->compare('firstStudent.ra',$this->firstStudent->ra,true,"OR");                  
                }                	

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        
		));
	}        
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Person the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        
        //*********Used in FIND method******************
       /*  public function scopes()
        {
            return array(
                'published'=>array(
                    'condition'=>'status=1',
                ), 
                'priority'=>array(
                    'order'=>'priority',
                    'limit'=>30,
                ),
            );
        } */
        public function limit_with_priority($limit=30)
        {
            $this->getDbCriteria()->mergeWith(array(
                //'order'=>'create_time DESC',
                //'limit'=>$limit,
                'order'=>'priority',
            ));
            return $this;
        }
        /*
        public function timeline(){
            $criteria=new CDbCriteria;
            $criteria->with=array(
                'firstProfessor','firstStudent','firstEmployee'
            );

            //$criteria->addBetweenCondition('firstProfessor.enterDate', strtotime(date('01/01/2010')), strtotime(date('01/01/2014')));
            //$criteria->addCondition('firstProfessor.enterDate > "2010-12-31" ', 'AND');
            //$criteria->addCondition('firstProfessor.exitDate < "31/12/2014" ', 'AND');
            
            
            //$criteria->addCondition('firstEmployee.registry > 1', 'OR');
            //$criteria->addCondition('firstStudent.ra > 1', 'OR');
            
            return Person::model()->limit_with_priority($limit=30)->findAll($criteria);
        }
        //************************************************/
        
        

        public function behaviors()
        {
            return array(
                'preview' => array(
                    'class' => 'ext.imageAttachment.ImageAttachmentBehavior',
                    // size for image preview in widget
                    'previewHeight' => 180,
                    'previewWidth' => 200,
                    // extension for image saving, can be also tiff, png or gif
                    'extension' => 'jpg',
                    // folder to store images
                    'directory' => Yii::getPathOfAlias('webroot').'/images/profile',
                    // url for images folder
                    'url' => Yii::app()->request->baseUrl . '/images/profile',
                    // image versions
                    'versions' => array(
                        'small' => array(
                            'resize' => array(200, null),
                        ),
                    )
                )
            );
        }  
        
       
}
