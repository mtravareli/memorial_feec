<?php

/**
 * This is the model class for table "professor".
 *
 * The followings are the available columns in table 'professor':
 * @property integer $id
 * @property string $enterDate
 * @property string $exitDate
 * @property string $academicFormation
 * @property string $researchArea
 * @property string $department
 * @property integer $idPerson
 * @property integer $registry
 *
 * The followings are the available model relations:
 * @property Person $idPerson0
 */
class Professor extends CActiveRecord
{
    
        public $isValid;
        public $person_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'professor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('registry', 'required'),
			array('idPerson, registry', 'numerical', 'integerOnly'=>true),
			array('academicFormation, researchArea, department', 'length', 'max'=>100),
			array('enterDate, exitDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, enterDate, exitDate, academicFormation, researchArea, department, idPerson, registry, person_search', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPerson0' => array(self::BELONGS_TO, 'Person', 'idPerson'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'enterDate' => 'Data de início',
			'exitDate' => 'Data de término',
			'academicFormation' => 'Formação acadêmica',
			'researchArea' => 'Área de pesquisa',
			'department' => 'Departamento',
			'idPerson' => 'Id Person',
			'registry' => 'Registro acadêmico',
                        'isValid' => 'É professor?'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array( 'idPerson0' );
                $criteria->compare( 'idPerson0.firstName', $this->person_search, true, 'OR' );
                $criteria->compare( 'idPerson0.middleName', $this->person_search, true, 'OR' );
                $criteria->compare( 'idPerson0.lastName', $this->person_search, true, 'OR' ); 
                
		$criteria->compare('id',$this->id);
		$criteria->compare('enterDate',$this->enterDate,true);
		$criteria->compare('exitDate',$this->exitDate,true);
		$criteria->compare('academicFormation',$this->academicFormation,true);
		$criteria->compare('researchArea',$this->researchArea,true);
		$criteria->compare('department',$this->department,true);
		$criteria->compare('idPerson',$this->idPerson);
		$criteria->compare('registry',$this->registry);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array(
                            'attributes'=>array(
                                'person_search'=>array(
                                    'asc'=>'idPerson0.firstName',
                                    'desc'=>'idPerson0.firstName DESC',
                                ),
                                '*',
                            ),
                        ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Professor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
