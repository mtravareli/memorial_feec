
-- Table: person
CREATE TABLE person ( 
    id           INTEGER         PRIMARY KEY AUTOINCREMENT,
    firstName    VARCHAR( 50 )   NOT NULL,
    middleName   VARCHAR( 100 ),
    lastName     VARCHAR( 50 )   NOT NULL,
    birthDate    DATE,
    deathDate    DATE,
    photo        VARCHAR( 100 ),
    contact      VARCHAR( 100 ),
    career       TEXT,
    observations TEXT,
    priority     INT( 11 ),
    ra           INT( 11 )       NOT NULL 
);


-- Table: employee
CREATE TABLE employee ( 
    id                INTEGER         PRIMARY KEY AUTOINCREMENT,
    enterDate         DATE,
    exitDate          DATE,
    academicFormation VARCHAR( 100 ),
    department        VARCHAR( 100 ),
    job               VARCHAR( 100 )  NOT NULL,
    idPerson          INTEGER         CONSTRAINT 'fk_person' REFERENCES person ( id ) ON DELETE RESTRICT
                                                               ON UPDATE RESTRICT 
);


-- Table: professor
CREATE TABLE professor ( 
    id                INTEGER         PRIMARY KEY AUTOINCREMENT,
    enterDate         DATE,
    exitDate          DATE,
    academicFormation VARCHAR( 100 ),
    researchArea      VARCHAR( 100 ),
    departament       VARCHAR( 100 ),
    idPerson          INTEGER         CONSTRAINT 'fk_person' REFERENCES person ( id ) ON DELETE RESTRICT
                                                               ON UPDATE RESTRICT 
);


-- Table: student
CREATE TABLE student ( 
    id               INTEGER        PRIMARY KEY AUTOINCREMENT,
    enterDate        DATE,
    exitDate         DATE,
    nickname         VARCHAR( 50 ),
    course           VARCHAR( 50 )  NOT NULL,
    class            INTEGER        NOT NULL,
    academicEntities TEXT,
    idPerson         INTEGER        CONSTRAINT 'fk_person' REFERENCES person ( id ) ON DELETE RESTRICT
                                                             ON UPDATE RESTRICT 
);

